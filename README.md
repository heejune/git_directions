git 사용법
=============
내가 기억하지 못하는 부분들을 정리해놓은 문서

기본 명령어
=============

* 저장소 생성
    <pre>git init</pre>

* 로컬 저장소로부터 복제
    <pre>git clone {local path}</pre>

* 원격 저장소로부터 복제
    <pre>git clone {url}</pre>

* 변경 사항 체크
    <pre>git status</pre>

* 특정 파일 스테이징
    <pre>git add {파일명}</pre>

* 변경된 모든 파일 스테이징
    <pre>git add *</pre>

* 커밋
    <pre>git commit -m "{변경 내용}"</pre>

* 원격으로 보내기
    <pre>git push origin master</pre>

* 원격저장소 추가
    <pre>git remote add origin {원격서버주소}</pre>

* 잘못된 add 삭제
    <pre>git rm -r --cached 폴더명</pre>

* git add 폴더/ 파일 제외 처리
    <pre>.gitignore 파일 생성 후 
폴더
    data/
파일
    test.php
    t*.php
</pre>


git config에 추가

<code>git config --global core.excludesfile ./.gitignore</code>


Commit
=============

* 커밋 합치기
    <pre>git rebase -i HEAD~4 // 최신 4개의 커밋을 하나로 합치기</pre>

* 커밋 메시지 수정
    <pre>git commit --amend // 마지막 커밋메시지 수정(ref) </pre>

* 간단한 commit 방법
    <pre>git add {변경한 파일명}
git commit -m "{변경 내용}"</pre>

* 커밋 이력 확인
    <pre>git log // 모든 커밋로그 확인
git log -3 // 최근 3개 커밋로그 확인
git log --pertty=oneline // 각 커밋을 한 줄로 표시</pre>

* 커밋 취소
    <pre>git reset HEAD^ // 마지막 커밋 삭제 
git reset --hard HEAD // 마지막 커밋 상태로 되돌림
git reset HEAD * // 스테이징을 언스테이징으로 변경</pre>


Branch
=============

* master 브랜치를 특정 커밋으로 옮기기
    <pre>git checkout better_branch
git merge --strategy=ours master
git checkout master
git merge better_branch</pre>

* 브랜치 목록
    <pre>git branch // 로컬
git branch -r // 리모트
git branch -a // 로컬, 리모트 포함된 모든 브랜치 보기</pre>

* 브랜치 생성
    <pre>git branch new master // master --> new 브랜치 생성
git push origin new // new 브랜치를 리모트로 보내기</pre>

* 브랜치 삭제
    <pre>git branch -D {삭제할 브랜치 명} // local
git push origin :{the_remote_branch} // remote</pre>

* 빈 브랜치 생성
    <pre>git checkout --orphan {새로운 브랜치 명}
git commit -a // 커밋해야 새로운 브랜치 생성됨
git checkout -b new-branch // 브랜치 생성과 동시에 체크아웃</pre>

* 리모트 브랜치 가져오기
    <pre>git checkout -t origin/{가져올 브랜치명} </pre>

* 브랜치 이름 변경
    <pre>git branch -m {new name}</pre>

Tag
=============

* 태그 조회하기
    <pre>git tag</pre>

* 태그 조회하기 (1.4.2 버전의 태그들만 검색하고 싶을때)
    <pre>git tag -l 'v1.4.2.*'</pre>

* 태그 붙이기
    <pre>git tag -a v1.4 -m "my version 1.4"</pre>

* 태그 정보, 커밋 정보 모두 조회
    <pre>git show v1.4</pre>

* 태그 공유하기
    <code>git push</code> 명령은 자동으로 리모트 서버에 태그를 전송하지 않는다. 태그를 만들었으면 서버에 별도록 push 해야 한다.
    <pre>git push origin v1.5</pre>
    만약  한번에 태그를 여러개 push 하고 싶다면 <code>--tags</code> 옵션을 추가하여 <code>git push</code>명령을 실행한다.
    <pre>git push origin --tags</pre>


* 참고 페이지
    <http://rogerdudler.github.com/git-guide/index.ko.html>




